# antiX-translation-info

This is a tiny script meant to display an informational message on antiX live systems on desktop after entering X screen, in case the system comes up still in English while user has chosen another language present in boot menu, to which it was not translated completely by now. The script is allowing to get disabled for next boots by user simply clicking the button »don't show again«.

This is not to be understood as a »Welcome-screen«, but as an information about the state of translation in the specific language and as an explanation why user faces an (at least partly) untranslated desktop when selecting “his” language from F2 boot menue instead of reading the expected familiar language. Hence it will be visible only in languages facing (many) missing important translations. There is an editable blocklist included inside the script alowing the maintainers to decide which language should see the message and which not.

It is meant to get included into ISO volumes of antiX for live usage.

## Usage
- The script file should be placed in some system folder used to store default executables like e.g. _/usr/local/bin_
- The advanced information ressource file _antiX-translation-info.pdf_ is expected to be found in the _.antiX-translation-info_ subfolder of the home directory of current user (e.g. _/home/demo/.antiX-translation-info_ on live Systems). If besides of the English version there are translated versions present, these will be used automatically, as long their filename is extended by the respective language Identifier between filename and extension. (e.g._antiX-translation-info.el.pdf_ or _antiX-translation-info.pt_BR.pdf_ ). Four significant as well as two character language code is accepted.
- The modified system file _desktop-session.conf_ has to be copied to the hidden subfolder _.desktop-session_ within home directory of current user (again _demo_ on an antiX live system) It contains the addition _STARTUP_DIALOG_CMD="translation-info.sh_.
- Any existing language files (_.mo_ type) go into the respective System folder (e.g. _/usr/share/locale/fr/LC_MESSAGES_) as usual.
- Please edit the configurable settings section within the script file according to your needs (e.g. blocklisted languages, resource path, tray icon choice)
- Accepted command line options: _--ignore-blocklist_   This switch allows to run the script even in environments with blocklisted languages like _»en«_.
- The button »Select language« allows the user to switch the information display and the advanced texts to another language than his user interface is set to. This is useful e.g. for people living in countries with more than one language, or in case the language originally selected was not translated yet, forcing a fallback to English, while the user would understand only French or Portuguese as a foreign language.

## Translation of the included texts
The translation resource is available at [antix-community-contributions](https://www.transifex.com/antix-linux-community-contributions/antix-contribs/antix-translation-info/) meanwhile for easier access to the language files. The .mo files compiled from the translations which were prepared completely already you may download from the locale subfolders, as well as the language template file _translation-info.sh.pot_  to create a specific language file (.po/.mo type). Also the pdf file containing advanced information is meant to get translated.
As long there is no .mo translation file for this script in a specific language present, the message screen itself as well as the advanced informational text (.pdf) will get displayed in English language (just like many parts of antiX in some languages) by default. Hint: Probably it is not necessary to translate this to languages of which it can be assumed that antiX ist translated to these already most completely, and which consequently get blocklisted anyway. 

## Authors and acknowledgment
This is an antiX community project. If you have any questions or suggestions, please feel free to write to [antiX-Forum](forum.antixlinux.com).

## License
GPL v.3

