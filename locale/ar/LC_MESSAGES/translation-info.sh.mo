��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  d   �     L  0   Y     �  
   �  O   �  	   	     	  ,   '	  5   T	     �	  H   �	  y   �	  @   k
     �
     �
     �
     �
  5   	  (   ?     h     �  .   �     �  P   �  5   9  M   o  *   �  A   �  B   *  &   m               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-23 22:26+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 بدلاً من ذلك ، يمكنك الاطلاع على بعض المعلومات المحلية. يستخدم هل تواجه مناطق غير مترجمة؟ اللغات المتوفرة: إحباط انقر فوق "متابعة" بعد إنشاء اتصال بالإنترنت. يغلق  خطأ في الشبكة إظهار المعلومات المحلية  هل ترى اللغة الإنجليزية فقط؟ لا تظهر مرة أخرى إذا كنت تفهم القليل من اللغة الإنجليزية إنه أسهل مما تعتقد ، ويمكنك المسا
همة بقدر ما تريد. فقط قم بالتسجيل. مساعدة الفريق المتطوع antiX للترجمة ، افعل ذلك. معلومات اللغة اختيار اللغة معلومة اضافية لا يوجد اتصال متاح بالإنترنت. الرجاء تحديد اللغة من  استمر امضي قدما اختيار اللغة وصولك إلى الفريق (مجانًا): يمكن تغيير ذلك. بالإضافة إلى لغتك الأم ، فيمكنك المساعدة في  لم تتم ترجمة antiX إلى لغتك بعد؟ التأكد من أنه يمكنك قريبًا تجربة antiX بلغتك. رؤية هذه المعلومات بها. للقيام بذلك ، انقر فوق الزر المقابل. اهلا وسهلا بكم للمساهمة في الترجمات. القائمة التي ترغب في  