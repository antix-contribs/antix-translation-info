��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  d   �     L  S   k     �     �  �   �     t	     �	  6   �	  =   �	  "   
  `   :
  �   �
  `   Z  !   �  $   �       )     6   @  ,   w     �     �  =   �  #     Z   /  D   �  b   �  -   2  N   `  J   �  ,   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:41+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Акрамя таго, вы можаце паглядзець мясцовую інфармацыю. Выкарыстоўвайце Вы сутыкаецеся з неперакладаемымі абласцямі? Даступныя мовы: Адмяніць Націсніце "Працягнуць" пасля таго, як усталюеце падключэнне да Інтэрнэту. Заключыць  Памылка сеткі Паказаць мясцовую інфармацыю Вы бачыце толькі англійскую мову? Больш не паказваць Калі вы крыху разумееце англійскую мову ў дадатак да Гэта прасцей, чым вы думаеце, і вы
можаце ўносіць столькі ці мала, колькі
хочаце. Проста зарэгіструйцеся. Дапамажыце валанцёрскай камандзе перакладчыкаў AntiX, Проста зрабі гэта. Інфармацыя пра мову выбар мовы дадатковая інфармацыя Няма падлучэння да Інтэрнэту. Калі ласка, абярыце мову Працягваць Выбар мовы Ваш доступ да каманды (бясплатна): Гэта можна змяніць. роднай мовы, вы можаце дапамагчы пераканацца, што antiX яшчэ не перакладзены на вашу мову? неўзабаве вы зможаце выпрабаваць антыХ на сваёй мове. бачыць гэтую інфармацыю. Для гэтага націсніце на адпаведную кнопку. Запрашаем унесці свой уклад у пераклады. з меню, на якім вы хочаце 