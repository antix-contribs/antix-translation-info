��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �  "   �  Z   �      	     )	  �   9	     �	  1   
  5   8
  D   n
  )   �
  �   �
  �   a  �   [     �       %     %   D  8   j  @   �     �  /     `   1  ?   �  �   �  c   i  |   �  F   J  v   �  x     I   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:24+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: bn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 বিকল্পভাবে, আপনি কিছু স্থানীয় তথ্য পরীক্ষা করে দেখতে পারেন। ব্যবহার করুন আপনি কি অনূদিত এলাকার সম্মুখীন হন? উপলব্ধ ভাষা: বাতিল আপনি একটি ইন্টারনেট সংযোগ স্থাপন করার পরে "চালিয়ে যান" এ ক্লিক করুন। উপসংহার  নেটওয়ার্ক ত্রুটি স্থানীয় তথ্য দেখান আপনি কি শুধু ইংরেজি দেখেন? আবার দেখাবেন না আপনি যদি আপনার মাতৃভাষা ছাড়াও একটু ইংরেজি বুঝেন, এটি আপনার ভাবার চেয়ে সহজ, এবং 
আপনি যতটা চান বা যত কম চান অবদান 
রাখতে পারেন। শুধু নিবন্ধন করুন। স্বেচ্ছাসেবক অ্যান্টিএক্স অনুবাদ দলকে সাহায্য করুন, এটা করতে. ভাষার তথ্য ভাষা নির্বাচন অতিরিক্ত তথ্য ইন্টারনেট সংযোগ নেই। অনুগ্রহ করে মেনু থেকে যে চোলতে থাকা ভাষা নির্বাচন করা দলে আপনার প্রবেশাধিকার (বিনামূল্যে): সেটা পরিবর্তন করা যায়। তাহলে আপনি আপনার ভাষায় শীঘ্রই অ্যান্টিএক্সের অভিজ্ঞতা antiX এখনও আপনার ভাষায় অনুবাদ করা হয়নি? পেতে পারেন তা নিশ্চিত করতে সাহায্য করতে পারেন। চান তা ভাষা নির্বাচন করুন। এটি করার জন্য, সংশ্লিষ্ট বোতামটি ক্লিক করুন। অনুবাদে অবদান রাখার জন্য আপনাকে খুবই স্বাগত। ভাষাটিতে আপনি এই তথ্য দেখতে 