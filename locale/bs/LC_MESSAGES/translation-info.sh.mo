��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  9   �     !  &   *     Q     b  >   j  
   �     �     �     �     �  '   	  ]   8	  0   �	     �	     �	     �	     �	     
     )
     B
     J
     X
     w
  $   �
  '   �
  &   �
       3     .   M     |               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:25+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternativno, možete pogledati neke lokalne informacije. Upotreba Nailazite li na neprevedena područja? Dostupni jezici: Prekini Kliknite na "Nastavi" nakon što uspostavite internetsku vezu. Zaključi  Greška na mreži Pokažite lokalne informacije Vidite li samo engleski? Ne prikazuj ponovo Ako razumijete malo engleskog osim svog Lakše je nego što mislite, a možete
doprinijeti koliko god želite.
Samo se registrirajte. Pomozite volonterskom timu za prevođenje AntiX, Samo to uradi. Jezičke informacije izbor jezika Dodatne informacije Internet veza nije dostupna. Molimo odaberite jezik s Nastavi Odabir jezika Vaš pristup timu (besplatan): To se može promijeniti. maternjeg jezika, možete pomoći da antiX još nije preveden na vaš jezik? uskoro doživite antiX na svom jeziku. vidjeti ove podatke. Da biste to učinili, kliknite odgovarajuće dugme. Vrlo ste dobrodošli da doprinesete prevodima. izbornika na kojem želite 