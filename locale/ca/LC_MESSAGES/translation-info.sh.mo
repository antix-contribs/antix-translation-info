��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  5   �       "   !     D     Y  L   a     �     �     �     �     �  1   	  a   B	  -   �	     �	     �	     �	     
  )   '
     Q
     h
     x
  (   �
     �
  6   �
  /   �
  0   /     `  +   u  (   �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:42+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Com a alternativa, podeu consultar informació local. Ús Us trobeu amb zones no traduïdes? Idiomes disponibles: Avortar Feu clic a "Continua" després d’haver establert una connexió a Internet. Conclou  Error de connexió Mostra informació local Només veieu anglès? No ho tornis a mostrar Si enteneu una mica d'anglès a més de la vostra És més fàcil del que penses i
pots contribuir tant o poc com vulguis.
Només cal registrar-se. Ajudeu l'equip voluntari de traducció antiX, Només fes-ho. Informació sobre l’idioma selecció d'idioma informació adicional No hi ha connexió a Internet disponible. Seleccioneu l’idioma Seguiu endavant Triar l’idioma El vostre accés a l’equip (gratuït): Això es pot canviar. llengua materna, podeu ajudar-vos a garantir que aviat antiX encara no s'ha traduït al vostre idioma? pugueu experimentar l'antiX en el vostre idioma. aquesta informació. Per fer-ho, feu clic al botó corresponent. Podeu contribuir molt a les traduccions. al menú en què voleu veure 