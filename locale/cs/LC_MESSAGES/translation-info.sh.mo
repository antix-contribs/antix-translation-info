��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  <   �     $  )   -     W     i  P   q     �     �     �     �     	  /   "	  w   R	  ;   �	     
     
     0
     ?
  *   P
     {
     �
     �
  !   �
     �
  0   �
  1     +   E     q  4   �  $   �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:24+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Můžete si také prohlédnout některé místní informace. Použít Setkáváte se s nepřeloženými úseky? Dostupné jazyky: Zrušit Po navázání internetového připojení klikněte na tlačítko "Pokračovat". Zavřít Chyba sítě Zobrazit místní informace Vidíte pouze angličtinu? Nezobrazovat znovu Pokud kromě své mateřštiny rozumíte trochu Je to jednodušší, než si myslíte,
a můžete přispět tolik nebo tak málo,
jak chcete. Stačí se zaregistrovat. Pomáhejte v dobrovolnickém překladatelském týmu antiX, Prostě to udělejte. Jazykové informace Výběr jazyka Více informací Není k dispozici připojení k internetu. Z nabídky vyberte jazyk, Pokračovat Výběr jazyka Váš přístup k týmu (zdarma): To lze změnit. anglicky, můžete přispět k tomu, že se brzy antiX ještě nebyl přeložen do vašeho jazyka? budete moci setkat s antiX ve svém jazyce. informace zobrazit. To provedete kliknutím na příslušné tlačítko. Na překladech se můžete podílet. ve kterém chcete tyto 