��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  7   �       4   +     `  	   {  ?   �  	   �     �     �  "   �     	  7   1	  w   i	  +   �	     
     
     0
     <
  %   R
     x
     �
     �
  ,   �
     �
  4   �
  -     '   M     u  (   �  2   �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:26+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Fel arall, gallwch edrych ar rywfaint o wybodaeth leol. Defnyddiwch Ydych chi'n dod ar draws ardaloedd heb eu cyfieithu? Yr ieithoedd sydd ar gael: Erthyliad Cliciwch ar "Parhau" ar ôl i chi sefydlu cysylltiad Rhyngrwyd. Casgliad  Gwall rhwydwaith Dangos gwybodaeth leol Ydych chi'n gweld Saesneg yn unig? Peidiwch â dangos eto Os ydych chi'n deall ychydig o Saesneg yn ychwanegol at Mae'n haws nag yr ydych chi'n meddwl,
a gallwch chi gyfrannu cymaint neu gyn lleied
ag y dymunwch. Cofrestrwch yn unig. Helpwch y tîm cyfieithu gwrthX gwirfoddol, Dim ond ei wneud. Gwybodaeth iaith dewis iaith Gwybodaeth Ychwanegol Nid oes cysylltiad rhyngrwyd ar gael. Dewiswch yr iaith o'r Daliwch ati Dewis yr iaith Eich mynediad i'r tîm (yn rhad ac am ddim): Gellir newid hynny. eich mamiaith, gallwch chi helpu i sicrhau y gallwch nad yw antiX wedi'i gyfieithu i'ch iaith eto? chi brofi gwrthX yn eich iaith yn fuan. weld y wybodaeth hon ynddi. I wneud hyn, cliciwch y botwm cyfatebol. Mae croeso mawr i chi gyfrannu at y cyfieithiadau. ddewislen yr hoffech chi 