��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  /   �       +        J     _  C   h     �     �     �     �     �  )   �  o   )	  5   �	     �	     �	     �	     
  /   
     C
     ]
     f
     r
     �
  .   �
  1   �
  +        2  1   M  2        �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:25+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternativt kan du se nogle lokale oplysninger. Anvend Støder du på afsnit, der ikke er oversat? Tilgængelige sprog: Annuller Klik på »Fortsæt«, når du har oprettet en internetforbindelse. Luk Netværksfejl Se lokale oplysninger Kan du kun se engelsk? Må ikke vises igen Hvis du forstår en smule engelsk ud over Det er nemmere, end du tror, og du kan
bidrage så meget eller så lidt, som du vil.
Du skal blot tilmelde dig. Hjælp til i det frivillige antiX-oversættelseshold, Bare gør det. Oplysninger om sprog Valg af sprog Flere oplysninger Der er ingen internetforbindelse til rådighed. Vælg venligst det sprog, Fortsæt Vælg sprog Din adgang til teamet (gratis): Dette kan ændres. dit modersmål, kan du være med til at sikre, antiX er endnu ikke blevet oversat til dit sprog? at du snart kan opleve antiX på dit sprog. oplysninger på, i menuen. Klik på den tilsvarende knap for at gøre dette. Du er velkommen til at deltage i oversættelserne. som du ønsker at se disse 