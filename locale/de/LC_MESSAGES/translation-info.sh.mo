��            )         �  1   �     �  "   �          !  :   (     c     i     z     �     �  4   �  B   �  *   6     a     v     �  !   �  #   �     �     �     �     
  ;     7   Z  )   �     �  *   �  8   �  #   8  f  \  @   �       *        8  	   N  U   X  
   �     �     �     �     �  -   	  �   @	  ?   �	     	
     
     )
  $   ?
  #   d
  
   �
     �
  !   �
     �
  6   �
  2     H   F     �  4   �  ?   �  !                                                           	                      
                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-22 16:25+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternativ können Sie sich einige lokale Informationen ansehen. Anwenden Begegnen Ihnen nicht übersetzte Bereiche? Verfügbare Sprachen: Abbrechen Klicken Sie auf »Fortfahren« nachdem Sie eine Internetverbindung hergestellt haben. Schließen Netzwerkfehler Lokale Informationen anzeigen Sie sehen nur Englisch? Nicht wieder zeigen Falls Sie neben Ihrer Muttersprache ein wenig Es ist einfacher  als man glaubt,  und  Sie  können so viel  oder so 
    wenig beitragen wie Sie möchten. Melden Sie sich einfach an. Helfen    Sie    im   ehrenamtlichen   antiX   Übersetzerteam, Sprachinformation Sprachauswahl Weitere Informationen Keine Internetverbindung verfügbar. Wählen Sie die Sprache, in der Sie Fortfahren Sprachauswahl Ihr Zugang zum Team (kostenfrei): Das läßt sich ändern. Englisch  verstehen,  können   Sie   dazu  beitragen, antiX wurde noch nicht in Ihre Sprache übersetzt? </u>daß      <u>Sie  antiX  bald  in  Ihrer  Sprache  erleben  können.   Klicken Sie dazu die entsprechende Schaltfläche an. Sie sind herzlich willkommen an den Übersetzungen mitzuwirken. diese Information sehen möchten. 