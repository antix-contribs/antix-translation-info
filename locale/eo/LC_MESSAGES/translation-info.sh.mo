��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  3   �       &        F     X  =   ^  	   �  
   �     �     �     �  *   �  p   !	  .   �	     �	     �	     �	     �	  #   
     &
     C
     L
      ^
     
  +   �
  0   �
  *   �
     !  ,   6  )   c     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:27+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternative vi povas rigardi iujn lokajn informojn. Uzu Ĉu vi renkontas netradukitajn areojn? Haveblaj lingvoj: Abort Alklaku "Daŭrigi" post kiam vi establis interretan konekton. Konkludi  Reta eraro Montru lokajn informojn Ĉu vi vidas nur la anglan? Ne montru denove Se vi komprenas iomete la anglan aldone al Ĝi estas pli facila ol vi pensas, kaj
vi povas kontribui tiom aŭ tiel malmulte
kiel vi volas. Nur registriĝu. Helpu la volontulan kontraŭX-tradukan teamon, Simple faru tion. Lingvaj informoj lingva elekto Kromaj Informoj Neniu interreta konekto disponebla. Bonvolu elekti la lingvon el Daŭrigu Elekti la lingvon Via aliro al la teamo (senpage): Tio povas esti ŝanĝita. via gepatra lingvo, vi povas helpi certigi, antiX ankoraŭ ne estis tradukita al via lingvo? ke vi baldaŭ spertos antiX en via lingvo. ĉi tiujn informojn. Por fari tion, alklaku la respondan butonon. Vi tre bonvenas kontribui al la tradukoj. la menuo, en kiu vi ŝatus vidi 