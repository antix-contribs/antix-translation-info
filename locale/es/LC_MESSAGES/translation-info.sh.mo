��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  �  �  #   R     v  6   ~     �     �  D   �     	     	     1	  F   L	     �	  7   �	  A   �	  %   
     B
     V
     s
     �
     �
     �
  	   �
     �
  $   �
       =   *  ,   h  5   �     �  #   �  C     '   J               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 19:13+0200
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: Spanish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Puede visualizar información local Aplicar ¿Algunas aplicaciones están parcialmente traducidas? Idiomas disponibles Cancelar Pulse sobre »Continuar« tras asegurar que está conectado a la red Cerrar Error de conexión Mostrar información local ¿Ves aplicaciones                
  sólo en Inglés?                 No me interesa Si comprendes inglés, y tienes dominio sobre tu idioma Puedes contribuir tanto como te permita tu tiempo o conocimiento. Colabore con el equipo de voluntarios Todo granito ayuda. Información sobre el Idioma Seleccionar idioma Más Información No hay conexión a Internet Por favor, seleccione el idioma Continuar Seleccionar idioma Puedes registrarte gratuitamente en: Aún hay esperanza. puedes contribuir a que estas aplicaciones estén en Español ¿Aún no se ha traducido antiX a tu idioma? y mejorar la experiencia de usuario de habla hispana. del menú desplegable. pulsando el botón correspondiente. que colaboran para hacer antiX accesible al público internacional. con el que se mostrará la información 