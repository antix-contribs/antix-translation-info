��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  1   �  	     &   #     J     c  A   j     �     �     �  "   �     	  *   	  j   A	  -   �	     �	     �	     
     $
  +   :
     f
     {
     �
     �
     �
  .   �
  -   �
  2   (     [  (   n  *   �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:42+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Bestela, tokiko informazio batzuk ikus ditzakezu. Erabilera Itzuli gabeko eremuak topatzen dituzu? Hizkuntza erabilgarriak: Abortu Egin klik "Jarraitu" botoian Interneteko konexioa ezarri ondoren. Amaitu  Sareko errorea Erakutsi tokiko informazioa Ingelesa bakarrik ikusten al duzu? Ez erakutsi berriro Zure ama hizkuntzaz gain ingeles pixka bat Uste baino errazagoa da, eta nahi
adina edo gutxien ekarpena egin dezakezu.
Izena ematea besterik ez dago. Lagundu boluntarioen antiX itzulpen taldeari, Egin ezazu eta kitto. Hizkuntzari buruzko informazioa hizkuntza hautatzea Informazio Gehigarria Ez dago Interneteko konexiorik erabilgarri. Informazio hau ikusi Segi aurrera Hizkuntza aukeratzea Taldearen sarbidea (doan): Hori alda daiteke. ulertzen baduzu, laster zure hizkuntzan antiXa antiX oraindik ez da zure hizkuntzara itzuli? bizi ahal izango duzula ziurtatzen lagun dezakezu. hautatu hizkuntza. Horretarako, egin klik dagokion botoian. Ongi etorriko zara itzulpenetan laguntzen. nahi duzun menuan 