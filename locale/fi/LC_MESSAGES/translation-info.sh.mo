��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  ?   �     '  $   +  (   P     y  ;   �     �     �     �     �     	  ,   	  j   H	  /   �	     �	     �	     �	     
  %   
     >
     P
     V
  (   d
     �
  *   �
  +   �
  +   �
     (     ?  .   ^     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:26+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Vaihtoehtoisesti voit tarkastella joitakin paikallisia tietoja. Hae Kohtaatko kääntämättömiä osia? Käytettävissä olevat kielet: - Kieli: Peruuta Napsauta »Jatka«, kun olet muodostanut Internet-yhteyden. Sulje Verkkovirhe Näytä paikalliset tiedot Näetkö vain englantia? Älä näytä uudelleen Jos ymmärrät äidinkielesi lisäksi hieman Se on helpompaa kuin luuletkaan,
ja voit osallistua niin paljon tai vähän
kuin haluat. Ilmoittaudu vain. Auta vapaaehtoisessa antiX-käännöstiimissä, Tee se vain. Kielitiedot Kielen valinta Lisätietoja Internet-yhteyttä ei ole saatavilla. Valitse valikosta Jatka Valitse kieli Pääset tutustumaan tiimiin (maksutta): Tämä voidaan muuttaa. englantia, voit auttaa varmistamaan, että antiX ei ole vielä käännetty kielellesi? voit pian kokea antiX:n omalla kielelläsi. nähdä nämä tiedot. Napsauta vastaavaa painiketta. Olet tervetullut osallistumaan käännöksiin. kieli, jolla haluat 