��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  �  �  9   5  	   o  5   y     �     �  :   �     	     	  !   #	  3   E	     y	  "   �	  F   �	  7   �	  
   +
     6
     Q
     i
  %   �
  .   �
  	   �
     �
     �
          ,  >   F  3   �     �  .   �  B     %   I               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-23 00:18+0200
Last-Translator: Robin, 2021
Language-Team: French (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Vous pouvez également consulter des informations locales Appliquer Vous êtes confrontés à des parties non traduites ? Langues disponibles : Annuler Cliquez sur "Procéder" après avoir établi la connexion. Fermer Erreur de connexion Afficher les informations locales Vous ne voyez que                    
  l'anglais ? Ne plus montrer Si vous comprenez un peu l'anglais Ce n'est pas difficile. Vous avancez à votre rythme (un peu ou plus). Rejoignez l'équipe de traducteurs bénévoles d'antiX, Faites-le. Informations linguistiques Sélection de la langue Pour plus d'informations Aucune connexion internet disponible. Veuillez sélectionner la langue dans laquelle Procéder Sélectionner la langue Inscrivez-vous gratuitement : On peut y remédier. vous pouvez contribuer et antiX n'est pas encore traduit dans votre langue préférée ? découvrir bientôt antiX dans votre propre langue. à partir du menu déroulant. au lieu d'appuyer sur le bouton correspondant. vous êtes toujours les bienvenus pour participer aux traductions. vous souhaitez lire cette information 