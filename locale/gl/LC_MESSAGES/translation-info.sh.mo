��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  7   �       &   #     J     `  H   h  
   �     �     �     �     �  0   	  f   9	  /   �	     �	     �	     �	     	
  )    
     J
     a
     p
  "   �
     �
  1   �
  -   �
  )        D  ,   W  -   �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:43+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Como alternativa, podes ver algunha información local. Uso ¿Atópaste con áreas non traducidas? Idiomas dispoñibles: Abortar Fai clic en "Continuar" despois de establecer unha conexión a Internet. Concluír  Erro de rede Mostrar información local ¿Só ves inglés? Non volva amosar Se comprende un pouco de inglés ademais da súa É máis doado do que pensas e podes
contribuír tanto ou menos como
queiras. Só tes que rexistrarte. Axude ao equipo voluntario de tradución antiX, Faino. Información sobre o idioma selección de idiomas Información adicional Non hai conexión a internet dispoñible. Seleccione o idioma no Seguir adiante Escolla do idioma O seu acceso ao equipo (de balde): Iso pódese cambiar. lingua materna, pode axudar a garantir que pronto antiX aínda non foi traducido ao seu idioma? poida experimentar o antiX no seu idioma. esta información. Para iso, fai clic no botón correspondente. É moi benvido a contribuír ás traducións. menú no que desexa ver 