��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �     p  v   �  #   �     	  �   4	     �	  %   �	  8   
  N   T
  )   �
  u   �
  �   C  o   C     �     �     �  (     V   .  8   �     �  =   �  X     (   o  }   �  s     t   �  ;   �  ]   ;  �   �  =                  
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:44+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 વૈકલ્પિક રીતે, તમે કેટલીક સ્થાનિક માહિતી જોઈ શકો છો. વાપરવુ શું તમે બિનઅનુવાદિત વિસ્તારોનો સામનો કરો છો? ઉપલબ્ધ ભાષાઓ: ગર્ભપાત તમે ઇન્ટરનેટ કનેક્શન સ્થાપિત કર્યા પછી "ચાલુ રાખો" પર ક્લિક કરો. નિષ્કર્ષ  નેટવર્ક ક્ષતિ સ્થાનિક માહિતી બતાવો શું તમે માત્ર અંગ્રેજી જુઓ છો? ફરી બતાવશો નહીં જો તમે તમારી માતૃભાષા ઉપરાંત થોડું અંગ્રેજી તે તમારા વિચારો કરતાં સહેલું છે,
અને તમે ઇચ્છો તેટલું અથવા ઓછું યોગદાન
આપી શકો છો. ફક્ત નોંધણી કરો. સ્વયંસેવક એન્ટીએક્સ અનુવાદ ટીમને મદદ કરો, બસ કરો. ભાષા માહિતી ભાષા પસંદગી વધારાની માહિતી કોઈ ઇન્ટરનેટ કનેક્શન ઉપલબ્ધ નથી. કૃપા કરીને મેનુમાંથી ચાલુ રાખો ભાષા પસંદ કરી રહ્યા છીએ ટીમમાં તમારી accessક્સેસ (નિ chargeશુલ્ક): તે બદલી શકાય છે. સમજો છો, તો તમે જલ્દી તમારી ભાષામાં એન્ટીએક્સનો antiX હજુ સુધી તમારી ભાષામાં અનુવાદિત નથી થયું? અનુભવ  શકો  તેની ખાતરી કરવામાં મદદ કરી શકો છો. આ માહિતી જોવા માંગો છો. આ કરવા માટે, અનુરૂપ બટન પર ક્લિક કરો. અનુવાદોમાં યોગદાન આપવા માટે તમારું ખૂબ સ્વાગત છે. ભાષા પસંદ કરો જેમાં તમે 