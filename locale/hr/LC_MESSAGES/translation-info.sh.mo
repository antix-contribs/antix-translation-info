��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  :   �  	   "  &   ,     S     d  ;   k     �     �     �     �     �  '   
	  ]   2	  1   �	     �	     �	     �	     �	     
     -
     D
     L
     Z
     y
  &   �
  '   �
  &   �
       2   %  0   X     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:28+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternativno, možete provjeriti neke lokalne informacije. Koristiti Nailazite li na neprevedena područja? Dostupni jezici: Prekid Kliknite "Nastavi" nakon što uspostavite internetsku vezu. Zaključiti  Pogreška mreže Pokažite lokalne podatke Vidite li samo engleski? Ne prikazuj više Ako razumijete malo engleskog osim svog Lakše je nego što mislite, a možete
doprinijeti koliko god želite.
Samo se registrirajte. Pomozite volonterskom prevoditeljskom timu AntiX, Jednostavno učinite to. Podaci o jeziku odabir jezika dodatne informacije Nije dostupna internetska veza. Molimo odaberite jezik Nastavi Odabir jezika Vaš pristup timu (besplatan): To se može promijeniti. materinskog jezika, možete pomoći da antiX još nije preveden na vaš jezik? uskoro doživite antiX na svom jeziku. želite vidjeti ove podatke. Da biste to učinili, kliknite odgovarajući gumb. Vrlo ste dobrodošli da doprinesete prijevodima. s izbornika na kojem 