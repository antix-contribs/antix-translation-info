��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  �  �  :   9     t  ?   �     �  	   �  M   �     ,	     3	  #   C	  $   g	     �	  6   �	  �   �	  1   X
     �
     �
     �
     �
     �
  +   �
  
          /   2     b  (   ~  7   �  *   �     
  %     :   2  &   m               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-22 23:28+0200
Last-Translator: Robin, 2021
Language-Team: Hungarian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Alternatívaként megnézhet néhány helyi információt. Alkalmazás Találkozik olyan területekkel, amelyeket nem fordítottak le? Elérhető nyelvek: Törölje Kattintson a "Folytatás" gombra, miután létrehozta az internetkapcsolatot. Zárva Hálózati hiba Helyi információk megjelenítése Csak angolul látod?                 Ne mutassa újra Ha az anyanyelvén kívül ért egy kicsit angolul is, Könnyebb, mint gondolnád, és annyit vagy annyit tehetsz
    hozzá, amennyit vagy amennyit csak szeretnél. Csak regisztráljon. Segíts az önkéntes antiX fordítói csapatban, Just do it. Hanginformáció Nyelvválasztás További információk Nincs internetkapcsolat. Kérjük, válassza ki, hogy milyen nyelven Folytatás Válasszon nyelvet Az Ön hozzáférése a csapathoz (ingyenesen): Ezt meg lehet változtatni. segíthet abban, hogy hamarosan a saját Az antiX-et még nem fordították le az Ön nyelvére? nyelvén is megtapasztalhassa az antiX-et.   Ehhez kattintson a megfelelő gombra. Szeretettel várjuk a fordításokban való részvételre. szeretné látni ezt az információt. 