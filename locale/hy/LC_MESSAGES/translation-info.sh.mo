��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  b   �     J  F   _      �     �  e   �     >	     R	  C   n	  ;   �	     �	  W   
  �   f
  D        Y  /   w  !   �  +   �  .   �  6   $     [  !   p  *   �      �  K   �  A   *  7   l  5   �  V   �  j   1  6   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:30+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: hy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Այլապես, կարող եք դիտել որոշ տեղական տեղեկություններ: Օգտագործել Հանդիպո՞ւմ եք չթարգմանված տարածքների: Մատչելի լեզուներ. Վիժեցնել Կտտացրեք «Շարունակել» ՝ ինտերնետ կապ հաստատելուց հետո: Եզրափակել  Ցանցային խնդիր Showույց տալ տեղական տեղեկատվությունը Դուք միայն անգլերեն տեսնու՞մ եք: Այլևս չցուցադրել Եթե ​​ձեր մայրենի լեզվից բացի մի փոքր անգլերեն Դա ավելի հեշտ է, քան կարծում եք,
և կարող եք նպաստել այնքան, որքան
քիչ կամենաք: Պարզապես գրանցվեք: Օգնեք կամավոր antiX թարգմանչական թիմին, Պարզապես արա դա. Լեզվական տեղեկատվություն լեզվի ընտրություն լրացուցիչ տեղեկություն Ինտերնետ կապ հասանելի չէ: Խնդրում ենք ընտրել այն լեզուն Շարունակիր Լեզվի ընտրություն Ձեր մուտքը թիմ (անվճար). Դա կարող է փոխվել: եք հասկանում, կարող եք օգնել երաշխավորել, antiX- ը դեռ չի՞ թարգմանվել ձեր լեզվով: որ շուտով ձեր լեզվով կզգաք antiX: տեսնել այս տեղեկատվությունը: Դա անելու համար կտտացրեք համապատասխան կոճակին: Դուք ողջունում եք ներդրում ունենալ թարգմանություններում: այն ցանկից, որում կցանկանայիք 