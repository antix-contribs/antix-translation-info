��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  �  �  9   7  	   q  $   {     �     �  D   �     	     	     	     5	     T	  4   f	  l   �	  /   
     8
     D
     ^
     u
  )   �
     �
     �
     �
  "   �
       7   7  4   o  '   �     �  0   �  /   �     /               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-22 23:33+0200
Last-Translator: Robin, 2021
Language-Team: Italian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 In alternativa, puoi guardare alcune informazioni locali. Applicare Incontra aree che non sono tradotte? Lingue disponibili: Cancella Clicca su »Continua« dopo aver stabilito una connessione Internet. Chiudere Errore di rete Mostra informazioni locali Vedi solo l'inglese?           Non mostrare più Se capite un po' di inglese oltre alla vostra lingua È più facile di quanto si pensi, e si può contribuire
    tanto o poco quanto si vuole. Basta iscriversi. Aiuta il team di traduttori volontari di antiX, Just do it. Informazioni sulla lingua Selezione della lingua Informazioni supplementari Nessuna connessione internet disponibile. Seleziona la lingua in cui vuoi Continua Seleziona la lingua Il tuo accesso al team (gratuito): Questo può essere cambiato. madre, potete contribuire a far sì che presto possiate antiX non è ancora stato tradotto nella tua lingua? sperimentare antiX nella vostra lingua.   Per farlo, cliccate sul pulsante corrispondente. Sei il benvenuto a partecipare alle traduzioni. vedere queste informazioni. 