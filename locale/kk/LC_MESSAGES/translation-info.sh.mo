��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  s   �     [  Q   j  #   �     �  p   �     `	     z	  <   �	  <   �	     
  [   *
  �   �
  N   %     t  "   �     �     �  +   �  *        >     S  @   k  )   �  O   �  K   &  S   r     �  ?   �  X     *   q               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:46+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Сонымен қатар, сіз кейбір жергілікті ақпаратты қарауға болады. Қолдану Сіз аударылмаған жерлерді кездестіресіз бе? Қол жетімді тілдер: Тоқтату Интернетке қосылғаннан кейін «Жалғастыру» түймесін басыңыз. Қорытындылау  Желілік қате Жергілікті ақпаратты көрсетіңіз Сіз тек ағылшын тілін көресіз бе? Қайта көрсетпеу Егер сіз ана тіліңізден басқа ағылшын тілін аздап Сіз ойлағаннан оңай, және
сіз қалағаныңызша көп немесе аз үлес
қоса аласыз. Тек тіркел. AntiX аударма еріктілер тобына көмектесіңіз, Жай ғана орында. Тіл туралы ақпарат тіл таңдау Қосымша Ақпарат Интернет байланысы жоқ. Мәзірден осы ақпаратты Жалғастыру Тілді таңдау Сіздің командаға кіруіңіз (ақысыз): Мұны өзгертуге болады. түсінсеңіз, жақын арада сіздің тілде antiX -ті antiX әлі сіздің тіліңізге аударылмаған ба? қолдана алатындығыңызға сенімді бола аласыз. таңдаңыз. Ол үшін сәйкес батырманы шертіңіз. Аудармаларға өз үлесіңізді қосуға қош келдіңіз. көргіңіз келетін тілді 