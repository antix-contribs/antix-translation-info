��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �  	   �  j   �     �     	  �   3	     �	  *   �	  9   )
  I   c
  !   �
  e   �
    5  _   Q  (   �     �  $   �  6     R   U  '   �     �  $   �  p     :     �   �  g   D  �   �  1   6  �   h  �   �  3   v               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:35+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: lo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 ອີກທາງເລືອກ ໜຶ່ງ, ເຈົ້າສາມາດເບິ່ງຂໍ້ມູນທ້ອງຖິ່ນບາງອັນ. ໃຊ້ ເຈົ້າພົບກັບພື້ນທີ່ທີ່ບໍ່ໄດ້ແປພາສາບໍ? ພາສາທີ່ມີ: ເອົາລູກອອກ ໃຫ້ຄລິກໃສ່ "ສືບຕໍ່" ຫຼັງຈາກທີ່ທ່ານໄດ້ສ້າງການເຊື່ອມຕໍ່ອິນເຕີເນັດ. ສະຫຼຸບ  ເຄືອຂ່າຍຜິດພາດ ສະແດງຂໍ້ມູນທ້ອງຖິ່ນ ເຈົ້າເຫັນແຕ່ພາສາອັງກິດບໍ? ຢ່າສະແດງອີກ ຖ້າເຈົ້າເຂົ້າໃຈພາສາອັງກິດ ໜ້ອຍ ໜຶ່ງ ມັນງ່າຍກວ່າທີ່ເຈົ້າຄິດ, ແລະເຈົ້າສາມາດປະກອບສ່ວນໄດ້ຫຼາຍຫຼື
ໜ້ອຍ ເທົ່າທີ່ເຈົ້າຕ້ອງການ. ພຽງແຕ່ລົງທະບຽນ. ຊ່ວຍເຫຼືອທີມງານແປພາສາ antiX ອາສາສະັກ, ພຽງແຕ່ເຮັດມັນ. ຂໍ້ມູນພາສາ ການເລືອກພາສາ ຂໍ້​ມູນ​ເພີ່ມ​ເຕີມ ບໍ່ມີການເຊື່ອມຕໍ່ອິນເຕີເນັດ. ກະລຸນາເລືອກພາ ສືບຕໍ່ໄປ ການເລືອກພາສາ ການເຂົ້າຫາທີມງານຂອງເຈົ້າ (ບໍ່ໄດ້ເສຍຄ່າ): ນັ້ນສາມາດປ່ຽນແປງໄດ້. ນອກຈາກພາສາແມ່ຂອງເຈົ້າ, ເຈົ້າສາມາດຊ່ວຍຮັບປະກັນວ່ antiX ຍັງບໍ່ທັນຖືກແປເປັນພາສາຂອງເຈົ້າບໍ? າເຈົ້າຈະສາມາດສໍາຜັດກັບ antiX ໃນພາສາຂອງເຈົ້າໃນໄວນີ້. ຢາກເຫັນຂໍ້ມູນນີ້. ເພື່ອເຮັດສິ່ງນີ້, ໃຫ້ຄລິກໃສ່ປຸ່ມທີ່ສອດຄ້ອງກັນ. ເຈົ້າຍິນດີຕ້ອນຮັບຫຼາຍທີ່ປະກອບສ່ວນເຂົ້າໃນການແປ. ສາຈາກເມນູທີ່ເຈົ້າ 