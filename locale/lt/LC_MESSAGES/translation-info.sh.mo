��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  <  �  4   �     �  4   �     /	  	   ?	  4   I	  	   ~	     �	     �	     �	     �	  -   �	  |   
  .   �
     �
     �
     �
     �
               0     7  2   K     ~  1   �  ,   �  -   �        ;   ;      w     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-22 23:37+0200
Last-Translator: Robin, 2021
Language-Team: Lithuanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
X-Generator: Poedit 2.3
 Taip pat galite peržiūrėti vietinę informaciją. Taikyti Ar susiduriate su sritimis, kurios nėra išverstos? Galimos kalbos: Atšaukti Užmezgę interneto ryšį spustelėkite »Tęsti«. Uždaryti Tinklo klaida Rodyti vietinę informaciją Ar matote tik anglų kalbą? Daugiau nerodyti Jei be gimtosios kalbos šiek tiek suprantate Tai paprasčiau, nei manote, ir galite prisidėti tiek, kiek
    norite, arba tiek, kiek norite. Tiesiog užsiregistruokite. Padėkite savanorių antiX vertėjų komandai, Just do it. Balso informacija Kalbos pasirinkimas Daugiau informacijos Interneto ryšio nėra. Iš meniu pasirinkite Tęsti Kalbos pasirinkimas Jūsų galimybė bendrauti su komanda (nemokamai): Tai galima pakeisti. ir anglų kalbą, galite padėti užtikrinti, kad antiX dar nėra išverstas į jūsų kalbą? netrukus galėsite naudotis antiX savo kalba. matyti šią informaciją. Norėdami tai padaryti, spustelėkite atitinkamą mygtuką. Kviečiame dalyvauti vertimuose. kalbą, kuria norite 