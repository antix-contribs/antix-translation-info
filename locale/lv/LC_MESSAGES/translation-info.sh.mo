��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  +   �  	     )        G     [  A   b     �     �     �  $   �     �  1   	  }   F	  5   �	     �	     
     &
     6
  #   L
     p
  	   �
     �
  &   �
     �
  ;   �
  %     0   >     o  8   �  3   �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:30+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Varat arī apskatīt vietējo informāciju. Piesakies Vai sastopaties ar netulkotām sadaļām? Pieejamās valodas: Atcelt Pēc interneta savienojuma izveides noklikšķiniet uz Turpināt. Aizvērt Tīkla kļūda Skatīt vietējo informāciju Vai jūs redzat tikai angļu valodu? Neparādiet vēlreiz Ja papildus dzimtās valodas zināšanām nedaudz Tas ir vienkāršāk, nekā domājat, un
varat ieguldīt tik daudz vai tik maz,
cik vēlaties. Vienkārši reģistrējieties. Palīdziet brīvprātīgo antiX tulkošanas komandā, Vienkārši to dariet. Valodas informācija Valodas izvēle Vairāk informācijas Interneta pieslēgums nav pieejams. Izvēlnē izvēlieties Turpināt Izvēlieties valodu Jūsu piekļuve komandai (bez maksas): To var mainīt. saprotat arī angļu valodu, varat palīdzēt nodrošināt, antiX vēl nav tulkots jūsu valodā? ka drīz varēsiet izmantot antiX savā valodā. redzēt šo informāciju. Lai to izdarītu, noklikšķiniet uz attiecīgās pogas. Jūs esat laipni aicināts piedalīties tulkojumos. valodu, kurā vēlaties 