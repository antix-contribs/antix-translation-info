��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  B   �     *  2   3     f     s  C   �     �     �  %   �     	     6	  ,   H	  �   u	  /   �	     +
     ;
     Y
     j
      �
     �
     �
     �
  +   �
       0      *   Q  1   |     �  <   �  :     !   @               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:35+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: mg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Raha tsy izany, azonao atao ny mijery vaovao sasany ao an-toerana. Ampiasao Mihaona amin'ny faritra tsy voadika teny ve ianao? Fiteny misy: Fanalan-jaza Tsindrio ny "Tohizo" aorian'ny nametrahanao fifandraisana Internet. Eo am-pamaranana  Hadisoana amin'ny tambajotra Asehoy ny fampahalalana eo an-toerana Anglisy ihany ve no hitanao? Aza aseho intsony Raha azonao ny teny anglisy kely ankoatry ny Mora noho ny nieritreretanao azy io, ary afaka mandray
anjara betsaka na kely araka izay tadiavinao ianao.
Misoratra anarana fotsiny. Ampio ny ekipa mpandika teny antiX an-tsitrapo, Ataovy fotsiny. Fampahalalana momba ny fiteny fifantina fiteny fampahafantarana fanampiny Tsy misy fifandraisana Internet. Safidio azafady ny fiteny Tohizo hatrany Misafidy ny fiteny Ny fidiranao amin'ny ekipa (maimaim-poana): Azo ovaina izany. tenin-dreninao dia azonao atao ny manampy antoka antiX mbola tsy nadika tamin'ny fiteninao? fa afaka mahatsapa antiX amin'ny fiteninao ianao. an'ity fampahalalana ity. Mba hanaovana izany dia kitiho ny bokotra mifanentana aminy. Tongasoa anao izahay handray anjara amin'ny fandikan-teny. avy amin'ny menio tianao hahitana 