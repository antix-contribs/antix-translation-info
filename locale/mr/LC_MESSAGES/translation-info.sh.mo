��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �     n  �   ~      $	     E	  �   \	     
  (   
  8   H
  S   �
  ,   �
         �  {   �          0     P  +   j  L   �  Q   �     5     O  l   o  $   �  �     p   �  �     3   �  j   �  �   6  F   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:47+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 वैकल्पिकरित्या, आपण काही स्थानिक माहिती पाहू शकता. वापरा तुम्हाला भाषांतर न झालेल्या क्षेत्रांचा सामना करावा लागतो का? उपलब्ध भाषा: रद्द करा आपण इंटरनेट कनेक्शन स्थापित केल्यानंतर "सुरू ठेवा" वर क्लिक करा. निष्कर्ष  नेटवर्क त्रुटी स्थानिक माहिती दाखवा तुम्हाला फक्त इंग्रजी दिसते का? पुन्हा दाखवू नका जर तुम्हाला तुमच्या मातृभाषेव्यतिरिक्त थोडेसे हे तुम्हाला वाटते त्यापेक्षा सोपे आहे आणि
तुम्हाला हवे तेवढे किंवा कमी योगदान
देऊ शकता. फक्त नोंदणी करा. स्वयंसेवक अँटीएक्स ट्रान्सलेशन टीमला मदत करा, फक्त ते करा. भाषा माहिती भाषा निवड अतिरिक्त माहिती इंटरनेट कनेक्शन उपलब्ध नाही. कृपया ज्या मेनूमध्ये तुम्हाला चालू ठेवा भाषा निवडणे कार्यसंघामध्ये तुमचा प्रवेश (विनामूल्य): ते बदलता येते. इंग्रजी समजत असेल, तर तुम्ही लवकरच तुमच्या भाषेत अँटीएक्स antiX चे अजून तुमच्या भाषेत भाषांतर झाले नाही? अनुभवू शकता याची खात्री करण्यात तुम्ही मदत करू शकता. भाषेतून भाषा निवडा. हे करण्यासाठी, संबंधित बटणावर क्लिक करा. भाषांतरांमध्ये योगदान देण्यासाठी आपले खूप स्वागत आहे. ही माहिती पाहायची आहे त्या 