��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  >   �     &  !   +     M     a  M   g     �     �     �     �     	  *   	  `   :	  /   �	     �	     �	     �	     
  2   *
  "   ]
     �
     �
  0   �
     �
  0   �
  2     7   O     �  6   �  >   �                    
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:37+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: mt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternattivament, tista 'tħares lejn xi informazzjoni lokali. Uża Tiltaqa 'ma' żoni mhux tradotti? Lingwi disponibbli: Abort Ikklikkja fuq "Kompli" wara li tkun stabbilixxejt konnessjoni ta 'l-Internet. Ikkonkludi  Żball fin-netwerk Uri informazzjoni lokali Tara l-Ingliż biss? Terġax turi Jekk tifhem ftit Ingliż minbarra l-lingwa Huwa iktar faċli milli taħseb, u tista 'tikkontribwixxi kemm trid kemm trid. Irreġistra biss. Għin lit-tim volontarju tat-traduzzjoni antiX, Taħsibhiex darbtejn. Informazzjoni lingwistika għażla tal-lingwa Informazzjoni addizzjonali L-ebda konnessjoni tal-internet mhija disponibbli. Jekk jogħġbok agħżel il-lingwa Ibqa 'għaddej L-għażla tal-lingwa L-aċċess tiegħek għat-tim (mingħajr ħlas): Dan jista 'jinbidel. materna tiegħek, tista 'tgħin biex tiżgura li antiX għadu ma ġiex tradott fil-lingwa tiegħek? dalwaqt tista' tesperjenza l-antiX fil-lingwa tiegħek. l-informazzjoni. Biex tagħmel dan, ikklikkja l-buttuna korrispondenti. Int mistieden ħafna li tikkontribwixxi għat-traduzzjonijiet. mill-menu li tixtieq tara din 