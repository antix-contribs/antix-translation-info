��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �  .   �  �   �  )   P	  (   z	  �   �	     n
  (   �
  M   �
  b   �
  1   b  �   �  I  $  w   n     �  "         #     :  R   W  3   �     �     �  S     H   `  �   �  w   O  �   �  J   ]  }   �  �   &  B   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:38+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 वैकल्पिक रूपमा, तपाइँ केहि स्थानीय जानकारी मा हेर्न सक्नुहुन्छ। प्रयोग गर्नुहोस् के तपाइँ अप्रत्याशित क्षेत्रहरुको सामना गर्नुहुन्छ? उपलब्ध भाषाहरु: रद्द गर्नुहोस् तपाइँ एक इन्टरनेट जडान स्थापित गरे पछि "जारी राख्नुहोस्" मा क्लिक गर्नुहोस्। निष्कर्ष  नेटवर्क त्रुटि स्थानीय जानकारी देखाउनुहोस् के तपाइँ मात्र अंग्रेजी देख्नुहुन्छ? फेरि नदेखाउनुहोस् यदि तपाइँ आफ्नो मातृभाषा को अतिरिक्त एक सानो अंग्रेजी यो तपाइँलाई सोच्नु भन्दा सजिलो छ,
र तपाइँ धेरै वा जति थोरै तपाइँ चाहानुहुन्छ
योगदान गर्न सक्नुहुन्छ। मात्र दर्ता गर्नुहोस्। स्वयंसेवक antiX अनुवाद टोलीलाई मद्दत गर्नुहोस्, मात्र गर। भाषा जानकारी भाषा चयन थप जानकारी कुनै इन्टरनेट जडान उपलब्ध छैन। कृपया मेनु बाट भाषा गरि राख भाषा छनौट टोलीमा तपाइँको पहुँच (नि: शुल्क): त्यो परिवर्तन गर्न सकिन्छ। बुझ्नुहुन्छ, तपाइँ यो सुनिश्चित गर्न मा मद्दत गर्न सक्नुहुन्छ antiX अझै सम्म तपाइँको भाषा मा अनुवाद गरिएको छैन? कि तपाइँ चाँडै तपाइँको भाषा मा antiX अनुभव गर्न सक्नुहुन्छ। जानकारी हेर्न चाहानुहुन्छ। यो गर्न को लागी, सम्बन्धित बटन क्लिक गर्नुहोस्। तपाइँ अनुवाद मा योगदान गर्न को लागी धेरै स्वागत छ। छान्नुहोस् जसमा तपाइँ यो 