��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  �  �  3   5     i  -   u     �  	   �  L   �     	     	     "	     9	     Y	  5   n	  n   �	  ,   
     @
     L
  	   ^
     h
     {
     �
     �
     �
  !   �
  !   �
  +     2   >  $   q     �  3   �  <   �                    
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-22 23:41+0200
Last-Translator: Robin, 2021
Language-Team: Dutch (https://www.transifex.com/antix-linux-community-contributions/teams/120110/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 U kunt ook kijken naar wat plaatselijke informatie. Solliciteer Komt u gebieden tegen die niet vertaald zijn? Beschikbare talen: Annuleren Klik op »Doorgaan« nadat u een internetverbinding tot stand hebt gebracht. Sluiten Netwerk fout Toon lokale informatie Zie je alleen Engels?           Niet meer laten zien Als u naast uw moedertaal een beetje Engels verstaat, Het is gemakkelijker dan u denkt, en u kunt zo veel
    of zo weinig bijdragen als u wilt. Meld je gewoon aan. Help in het vrijwillige antiX vertalersteam, Just do it. Spraak informatie Taalkeuze Verdere informatie Er is geen internetverbinding. Selecteer in het menu de taal Doorgaan Kies de taal Uw toegang tot het team (gratis): Dat kan worden veranderd.         kunt u er mede voor zorgen dat u binnenkort antiX is nog niet vertaald naar uw favoriete taal? antiX in uw eigen taal kunt ervaren. wenst te zien. Om dit te doen, klikt u op de overeenkomstige knop. U bent van harte welkom om deel te nemen aan de vertalingen. waarin u deze informatie 