��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  ,   �       #        =     S  H   [     �     �     �     �     �  '   �  d    	  0   �	     �	     �	  
   �	     �	  (   �	      
     2
     ;
      H
     i
  (   y
  6   �
  -   �
       7     :   T     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:31+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: nn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternativt kan du se på lokal informasjon. Bruk Støter du på uoversatte områder? Tilgjengelige språk: Avbryte Klikk på "Fortsett" etter at du har opprettet en Internett -tilkobling. Konkludere  Nettverksfeil Vis lokal informasjon Ser du bare engelsk? Ikke vis igjen Hvis du forstår litt engelsk i tillegg Det er lettere enn du tror, ​​og du
kan bidra så mye eller så lite du vil.
Bare registrer deg. Hjelp det frivillige antiX -oversettelsesteamet, Bare gjør det. Språkinformasjon språkvalg tilleggsinformasjon Ingen internettforbindelse tilgjengelig. Velg språket fra Fortsett Velge språk Din tilgang til teamet (gratis): Det kan endres. til morsmålet ditt, kan du bidra til at har antiX ennå ikke blitt oversatt til språket ditt? du snart kan oppleve antiX på språket ditt. denne informasjonen. For å gjøre dette, klikk på den tilsvarende knappen. Du er hjertelig velkommen til å bidra til oversettelsene. menyen der du vil se 