��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �  *   }  s   �      	     =	  �   V	     
  *   
  @   E
  G   �
  C   �
  r     �   �  �   ~  "   	     ,     I  (   `  g   �  <   �     .     B  N   b  F   �  �   �  w   z  �   �  F   }  k   �  p   0  ?   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:32+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: or
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 ବ ly କଳ୍ପିକ ଭାବରେ, ଆପଣ କିଛି ସ୍ଥାନୀୟ ସୂଚନା ଯାଞ୍ଚ କରିପାରିବେ | ବ୍ୟବହାର କରନ୍ତୁ | ତୁମେ ଅନୁବାଦ ହୋଇନଥିବା ଅଞ୍ଚଳର ସାମ୍ନା କରୁଛ କି? ଉପଲବ୍ଧ ଭାଷା: ପରିତ୍ୟାଗ ଆପଣ ଏକ ଇଣ୍ଟରନେଟ୍ ସଂଯୋଗ ସ୍ଥାପନ କରିବା ପରେ “ଜାରି” ଉପରେ କ୍ଲିକ୍ କରନ୍ତୁ | ଶେଷ କର  ନେଟୱର୍କ ତ୍ରୁଟି | ସ୍ଥାନୀୟ ସୂଚନା ଦେଖାନ୍ତୁ | ଆପଣ କେବଳ ଇଂରାଜୀ ଦେଖନ୍ତି କି? ପୁନର୍ବାର ଦେଖାନ୍ତୁ ନାହିଁ | ଯଦି ତୁମେ ତୁମର ମାତୃଭାଷା ବ୍ୟତୀତ ଟିକିଏ ଇଂରାଜୀ ତୁମେ ଭାବିବା ଅପେକ୍ଷା ଏହା ସହଜ, ଏବଂ
ତୁମେ ଯେତିକି ଚାହିଁବ ସେତିକି ଯୋଗଦାନ
ଦେଇ ପାରିବ | କେବଳ ପଞ୍ଜିକରଣ କର | ସ୍ୱେଚ୍ଛାସେବୀ ଆଣ୍ଟିଏକ୍ସ ଅନୁବାଦ ଦଳକୁ ସାହାଯ୍ୟ କରନ୍ତୁ, କେବଳ ତାହା କର | ଭାଷା ସୂଚନା ଭାଷା ଚୟନ ଅତିରିକ୍ତ ସୂଚନା କ internet ଣସି ଇଣ୍ଟରନେଟ୍ ସଂଯୋଗ ଉପଲବ୍ଧ ନାହିଁ | ଦୟାକରି ମେନୁରୁ ଭାଷା ଚୟନ ଜାରି ରଖ ଭାଷା ବାଛିବା ଦଳକୁ ଆପଣଙ୍କର ପ୍ରବେଶ (ମାଗଣାରେ): ତାହା ପରିବର୍ତ୍ତନ ହୋଇପାରିବ | ବୁ understand ିଛ, ତୁମେ ନିଶ୍ଚିତ କରିବାରେ ସାହାଯ୍ୟ କରିପାରିବ antiX ଏପର୍ଯ୍ୟନ୍ତ ଆପଣଙ୍କ ଭାଷାରେ ଅନୁବାଦ ହୋଇନାହିଁ? ଯେ ତୁମେ ଶୀଘ୍ର ତୁମର ଭାଷାରେ ଆଣ୍ଟିଏକ୍ସ ଅନୁଭବ କରିପାରିବ | ସୂଚନା ଦେଖିବାକୁ ଚାହାଁନ୍ତି | ଏହା କରିବାକୁ, ସଂପୃକ୍ତ ବଟନ୍ କ୍ଲିକ୍ କରନ୍ତୁ | ଅନୁବାଦରେ ଯୋଗଦାନ କରିବାକୁ ତୁମେ ବହୁତ ସ୍ୱାଗତ | କରନ୍ତୁ ଯେଉଁଥିରେ ଆପଣ ଏହି 