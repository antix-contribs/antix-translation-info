��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  w   �     _  n   l  )   �     	  �   "	     �	  "   �	  5   �	  T   $
  )   y
  �   �
  �   1  d   ,     �  ,   �      �  "   �  Z     C   y     �      �  F   �  <   ?  �   |  �   	  �   �  J   D  `   �  �   �  C   u               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:39+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 ਇਸ ਦੇ ਉਲਟ, ਤੁਸੀਂ ਕੁਝ ਸਥਾਨਕ ਜਾਣਕਾਰੀ ਦੇਖ ਸਕਦੇ ਹੋ. ਵਰਤੋ ਕੀ ਤੁਸੀਂ ਅਨੁਵਾਦਤ ਖੇਤਰਾਂ ਦਾ ਸਾਹਮਣਾ ਕਰਦੇ ਹੋ? ਉਪਲਬਧ ਭਾਸ਼ਾਵਾਂ: ਅਧੂਰਾ ਛੱਡੋ ਇੰਟਰਨੈਟ ਕਨੈਕਸ਼ਨ ਸਥਾਪਤ ਕਰਨ ਤੋਂ ਬਾਅਦ "ਜਾਰੀ ਰੱਖੋ" ਤੇ ਕਲਿਕ ਕਰੋ. ਸਿੱਟਾ  ਨੈੱਟਵਰਕ ਗੜਬੜ ਸਥਾਨਕ ਜਾਣਕਾਰੀ ਦਿਖਾਓ ਕੀ ਤੁਸੀਂ ਸਿਰਫ ਅੰਗਰੇਜ਼ੀ ਵੇਖਦੇ ਹੋ? ਦੁਬਾਰਾ ਨਾ ਦਿਖਾਓ ਜੇ ਤੁਸੀਂ ਆਪਣੀ ਮਾਂ ਬੋਲੀ ਤੋਂ ਇਲਾਵਾ ਥੋੜ੍ਹੀ ਜਿਹੀ ਅੰਗਰੇਜ਼ੀ ਇਹ ਤੁਹਾਡੇ ਸੋਚਣ ਨਾਲੋਂ ਸੌਖਾ ਹੈ,
ਅਤੇ ਤੁਸੀਂ ਜਿੰਨਾ ਚਾਹੋ ਘੱਟ ਜਾਂ ਘੱਟ
ਯੋਗਦਾਨ ਪਾ ਸਕਦੇ ਹੋ. ਸਿਰਫ ਰਜਿਸਟਰ ਕਰੋ. ਵਲੰਟੀਅਰ ਐਂਟੀਐਕਸ ਅਨੁਵਾਦ ਟੀਮ ਦੀ ਮਦਦ ਕਰੋ, ਇਸ ਨੂੰ ਕਰੋ. ਭਾਸ਼ਾ ਦੀ ਜਾਣਕਾਰੀ ਭਾਸ਼ਾ ਦੀ ਚੋਣ ਵਧੀਕ ਜਾਣਕਾਰੀ ਕੋਈ ਇੰਟਰਨੈਟ ਕਨੈਕਸ਼ਨ ਉਪਲਬਧ ਨਹੀਂ ਹੈ. ਕਿਰਪਾ ਕਰਕੇ ਉਸ ਮੀਨੂ ਵਿੱਚੋਂ ਚੱਲਦੇ ਰਹੋ ਭਾਸ਼ਾ ਦੀ ਚੋਣ ਟੀਮ ਤੱਕ ਤੁਹਾਡੀ ਪਹੁੰਚ (ਮੁਫਤ): ਇਸ ਨੂੰ ਬਦਲਿਆ ਜਾ ਸਕਦਾ ਹੈ. ਸਮਝਦੇ ਹੋ, ਤਾਂ ਤੁਸੀਂ ਇਹ ਯਕੀਨੀ ਬਣਾਉਣ ਵਿੱਚ ਸਹਾਇਤਾ ਕਰ ਸਕਦੇ ਐਂਟੀਐਕਸ ਦਾ ਅਜੇ ਤੱਕ ਤੁਹਾਡੀ ਭਾਸ਼ਾ ਵਿੱਚ ਅਨੁਵਾਦ ਨਹੀਂ ਕੀਤਾ ਗਿਆ ਹੈ? ਹੋ ਕਿ ਤੁਸੀਂ ਜਲਦੀ ਹੀ ਆਪਣੀ ਭਾਸ਼ਾ ਵਿੱਚ ਐਂਟੀਐਕਸ ਦਾ ਅਨੁਭਵ ਕਰ ਸਕੋ. ਇਹ ਜਾਣਕਾਰੀ ਵੇਖਣਾ ਚਾਹੁੰਦੇ ਹੋ. ਅਜਿਹਾ ਕਰਨ ਲਈ, ਅਨੁਸਾਰੀ ਬਟਨ ਤੇ ਕਲਿਕ ਕਰੋ. ਅਨੁਵਾਦਾਂ ਵਿੱਚ ਯੋਗਦਾਨ ਪਾਉਣ ਲਈ ਤੁਹਾਡਾ ਬਹੁਤ ਸਵਾਗਤ ਹੈ. ਭਾਸ਼ਾ ਚੁਣੋ ਜਿਸ ਵਿੱਚ ਤੁਸੀਂ 