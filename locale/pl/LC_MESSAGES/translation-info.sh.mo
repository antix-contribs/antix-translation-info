��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  G  �  ;   �     	  7   	     D	     W	  B   ^	     �	     �	     �	     �	     �	  ;   
  �   =
  "   �
     �
     �
     
       -   +     Y  	   s     }  (   �     �  <   �  :     5   A     w  4   �  (   �     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-22 22:50+0200
Last-Translator: Robin, 2021
Language-Team: Polish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
X-Generator: Poedit 2.3
 Ewentualnie można zapoznać się z informacjami lokalnymi. Aplikuj Czy napotykasz obszary, które nie są przetłumaczone? Dostępne języki: Anuluj Po nawiązaniu połączenia z Internetem kliknij na »Kontynuuj«. Zamknij Błąd sieciowy Pokaż informacje lokalne Widzisz tylko angielski? Nie pokazuj ponownie Jeśli oprócz swojego języka ojczystego rozumiesz trochę To łatwiejsze niż myślisz i możesz wnieść
    swójwkład tak dużo lub tak mało, jak chcesz.
    Wystarczy się zapisać. Pomóż w zespole tłumaczy antiX, Po prostu zrób to. Informacje głosowe Wybór języka Dalsze informacje Brak możliwości podłączenia do internetu. Wybierz język, w którym Kontynuuj Wybierz język Twój dostęp do zespołu (bezpłatnie): To można zmienić. angielskiego, możesz przyczynić się do tego, że wkrótce antiX nie został jeszcze przetłumaczony na Twój język? będziesz mógł doświadczać antiX w swoim języku. te informacje. W tym celu należy kliknąć na odpowiedni przycisk. Zapraszamy do udziału w tłumaczeniach. mają być wyświetlane 