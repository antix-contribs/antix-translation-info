��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  ]   �  
   E  M   P     �     �  y   �  
   .	     9	  )   T	  4   ~	     �	  b   �	  �   4
  D   �
     >     R     k     �  7   �  '   �     �     �  /        G  R   a  C   �  Q   �  3   J  J   ~  `   �  +   *               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:39+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: ps
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 په بدیل توګه ، تاسو کولی شئ ځینې ځایی معلومات وګورئ. کارول ایا تاسو له نه ژباړل شویو سیمو سره مخ یاست؟ شته ژبې: سقط وروسته لدې چې تاسو د انټرنیټ اتصال رامینځته کړی په "دوام" کلیک وکړئ. پایله د شبکې تېروتنه ځایی معلومات ښکاره کړئ ایا تاسو یوازې انګلیسي ګورئ؟ بیا مه ښکاره کوئ که تاسو د خپلې مورنۍ ژبې سربیره یو څه انګلیسي پوهیږئ ،  دا ستاسو له فکر کولو څخه اسانه دی ،
او تاسو کولی شئ څومره یا څومره چې وغواړئ nمرسته وکړئ. یوازې نوم لیکنه وکړئ. د داوطلب ضد ژباړې ټیم سره مرسته وکړئ ، بس دا وکړه. د ژبې معلومات د ژبې انتخاب نور معلومات هیڅ انټرنیټ پیوستون شتون نلري. مهرباني وکړئ هغه مینو ځه د ژبې غوره کول ټیم ته ستاسو لاسرسی (وړیا): دا بدلیدلی شي. تاسو کولی شئ ډاډ ترلاسه کولو کې مرسته وکړئ چې  antiX لاهم ستاسو ژبې ته نه دی ژباړل شوی؟ تاسو به ژر په خپله ژبه کې انټي ایکس تجربه کړئ. تاسو غواړئ دا معلومات وګورئ. د دې کولو لپاره ، اړونده ت buttonۍ کلیک وکړئ. تاسو ته ښه راغلاست ویل کیږي چې په ژباړو کې برخه واخلئ. څخه ژبه غوره کړئ چیرې چې 