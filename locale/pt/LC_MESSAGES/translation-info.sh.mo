��          �      ,      �  "   �     �     �     �  4   �  B     *   T          �     �     �     �  ;   �  7     )   K  8   u  �  �      �     �     �     �  >   �  ;     7   Y     �     �     �     �      �  0     5   A  .   w  +   �                                 
                                         	    Are you facing untranslated parts? Close Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information More Information Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. you are always welcome to take part in the translations. Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-23 00:26+0200
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2021
Language-Team: Portuguese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Aparecem partes não traduzidas? Fechar Está tudo em inglês?      Não mostrar novamente Caso o utilizador saiba inglês, além da sua língua materna, Não é difícil e pode fazer apenas o que quiser ou puder. Junte-se à equipa de tradutores voluntários do antiX, Basta pôr-se a isso. Informação sobre o idioma Mais informação Registe-se gratuitemente: Isso pode ser solucionado.       pode contribuir para a tradução, podendo assim O antiX ainda não está traduzido para o seu idioma? usar o antiX na sua própria língua em breve. agradecemos que tome parte nas traduções. 