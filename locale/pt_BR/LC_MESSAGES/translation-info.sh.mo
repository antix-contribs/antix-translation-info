��          �   %   �      @  1   A  "   s     �  :   �     �     �     �     	     "  4   3  B   h  *   �     �     �     �  !        *     2     D  ;   X  7   �  )   �  *   �  8   !  �  Z  :   C  D   ~     �  D   �                9  <   X     �  ;   �  R   �  7   :	     r	  $   �	     �	  -   �	  	   �	     �	     
  ;   (
  B   d
  >   �
  2   �
  7                                                                     
                                            	                Alternatively you may view some local information Are you facing untranslated parts? Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information More Information No internet connection available. Procede Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. instead by pressing the respective button. you are always welcome to take part in the translations. Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-22 22:34+0200
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2021
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Opcionalmente, você pode ver algumas informações locais Você está tendo dificuldades
    com os programas não traduzidos? Cancelar Clique em »Continuar« após estabelecer a conexão com a internet. Fechar Falha na conexão com a internet Exibir as Informações Locais Você vê apenas textos
                  em idioma Inglês? Não Exibir Novamente Caso você entenda além da sua língua nativa, um pouco do Isso não é difícil e você pode fazer muito ou pouco,
    só depende de você. Junte-se à equipe de tradutores voluntários do antiX, Apenas faça. Informações Sobre o Idioma/Língua Mais Informações Não há conexão disponível com a internet. Continuar Inscreva-se gratuitamente: Saiba que você pode ajudar. idioma Inglês, você também pode contribuir, então você O antiX ainda não foi totalmente traduzido
    para o seu idioma? poderá experimentar em breve o antiX em sua própria língua. ao invés disso, pressionando o respectivo botão. você é sempre bem-vindo a participar das traduções. 