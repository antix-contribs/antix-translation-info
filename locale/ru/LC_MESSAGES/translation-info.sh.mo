��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  @  �  [   �     	  X   0	     �	     �	  �   �	     @
     O
  4   e
  8   �
  $   �
  \   �
  �   U  e        j  "   �     �  !   �  4   �  <        T     i  ;   �  #   �  c   �  :   G  �   �     ,  R   I  T   �  7   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-22 16:55+0200
Last-Translator: Robin, 2021
Language-Team: Russian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 2.3
 Или вы можете ознакомиться с местной информацией. Применить Встречаете ли вы области, которые не переведены? Доступные языки: Отменить Нажмите кнопку »Продолжить« после установления соединения с Интернетом. Закрыть Ошибка сети Показать местную информацию Вы видите
    только английский? Не показывать снова Если вы понимаете немного английский в дополнение Это проще, чем вы думаете, и вы можете потратить
    столько или меньше времени, сколько захотите. Присоединяйтесь к команде волонтеров-переводчиков antiX, Просто делайте. Информация о языке Выбор языка Больше информации Подключения к Интернету нет. Пожалуйста, выберите в меню язык, Продолжать Выберите язык Ваш доступ к команде (бесплатно): Это можно изменить. к своему родному языку, вы можете способствовать тому, antiX ещё не переведён на ваш язык? </u>что в скором времени у вас будет возможность
    <u>познакомиться с antiX на вашем родном языке. эту информацию. Для этого нажмите на соответствующую кнопку. Приветствуем Вас принять участие в переводах. на котором вы хотели бы видеть 