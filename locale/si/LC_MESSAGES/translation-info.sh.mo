��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �     v  u   �  +   �     (	  �   H	  #   �	     !
  >   8
  O   w
  ,   �
  �   �
  "  w  �   �     9  "   Q  )   t  "   �  T   �  2        I  ,   f  Q   �  .   �  �        �  s      #   �  x   �     1  /   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:41+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: si
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 විකල්පයක් ලෙස, ඔබට සමහර දේශීය තොරතුරු දෙස බැලිය හැකිය. භාවිත ඔබ පරිවර්‍තනය නොකළ ප්‍රදේශවලට මුහුණ දෙනවාද? ලබා ගත හැකි භාෂා: ගබ්සා කරන්න ඔබ අන්තර්ජාල සම්බන්ධතාවක් ඇති කරගත් පසු "ඉදිරියට යන්න" ක්ලික් කරන්න. නිගමනය කරන්න  ජාල දෝෂය දේශීය තොරතුරු පෙන්වන්න ඔබට පෙනෙන්නේ ඉංග්‍රීසි විතරද? ආයේ පෙන්නන්න එපා ඔබේ මව් භාෂාවට අමතරව ඔබට ඉංග්‍රීසි ටිකක් තේරෙනවා එය ඔබ සිතනවාට වඩා පහසු වන අතර ඔබට අවශ්‍ය ප්‍රමාණයට
හෝ සුළු ප්‍රමාණයකට දායක විය හැකිය.
ලියාපදිංචි වෙන්න විතරයි. ස්වේච්ඡා සේවයේ යෙදෙන විරෝධී පරිවර්‍තන කණ්ඩායමට උදව් කරන්න, එය කරන්න. භාෂා තොරතුරු භාෂා තෝරා ගැනීම අමතර තොරතුරු අන්තර්ජාල සම්බන්ධතාවයක් නොමැත. මෙම තොරතුරු බැලීමට දිගටම යන්න භාෂාව තෝරා ගැනීම කණ්ඩායමට ඔබේ ප්‍රවේශය (නොමිලේ): එය වෙනස් කළ හැකිය. නම්, ඔබට ඉක්මනින්ම ඔබේ භාෂාවෙන් ඇන්ටීඑක්ස් අත්දැකීම ඇන්ටීඑක්ස් තවමත් ඔබේ භාෂාවට පරිවර්තනය කර නැතිද? ලබා ගත හැකි බවට සහතික වීමට ඔබට උදවු කළ හැකිය. භාෂාව තෝරන්න. මෙය සිදු කිරීම සඳහා, අදාළ බොත්තම ක්ලික් කරන්න. පරිවර්තන සඳහා දායක වීමට ඔබව සාදරයෙන් පිළිගනිමු. ඔබ කැමති මෙනුවෙන් 