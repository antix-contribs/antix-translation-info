��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  1   �       !   !     C  	   Z  8   d     �     �     �     �     �  -   	  k   2	  3   �	     �	     �	     �	     
      
     =
     V
     _
  #   n
     �
  0   �
  '   �
  ,        2  +   B  &   n     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:34+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Ogledate si lahko tudi nekaj lokalnih informacij. Uporabi Ali naletite na neprevedene dele? Razpoložljivi jeziki: Prekliči Ko vzpostavite internetno povezavo, kliknite "Nadaljuj". Zapri Napaka v omrežju Oglejte si lokalne informacije Ali vidite samo angleščino? Ne prikaži znova Če poleg maternega jezika razumete tudi malo To je lažje, kot si mislite,
in lahko prispevate toliko ali manj,
kolikor želite. Preprosto se prijavite. Pomagajte v prostovoljni ekipi za prevajanje antiX, Preprosto to storite. Jezikovne informacije Izbira jezika Več informacij Internetna povezava ni na voljo. V meniju izberite jezik, Nadaljuj Izberite jezik vaš dostop do ekipe (brezplačno): To je mogoče spremeniti. angleščine, lahko pripomorete k temu, da boste antiX še ni bil preveden v vaš jezik? lahko kmalu doživeli antiX v svojem jeziku. te informacije. To storite tako, da kliknete ustrezen gumb. Vabljeni k sodelovanju pri prevajanju. v katerem želite videti 