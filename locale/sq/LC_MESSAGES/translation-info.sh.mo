��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  7   �  	     $   )     N     g  A   n     �     �     �     �     	  +   	  z   C	  0   �	     �	     �	     
     *
  $   =
     b
     �
     �
  !   �
     �
  &   �
  3   �
  .   1     `  6     <   �      �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:48+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Përndryshe, mund të shikoni disa informacione lokale. Përdorni A hasni në zona të pa përkthyera? Gjuhët në dispozicion: Aborto Klikoni në "Vazhdo" pasi të keni krijuar një lidhje interneti. Përfundoni  Gabim në rrjet Shfaq informacionin lokal A shihni vetëm anglisht? Mos u shfaq përsëri Nëse kuptoni pak anglisht përveç gjuhës Easiershtë më e lehtë nga sa mendoni,
dhe ju mund të kontribuoni sa më shumë ose
sa të doni. Thjesht regjistrohuni. Ndihmoni ekipin vullnetar të përkthimit antiX, Vetëm Bëje. Informacion gjuhësor përzgjedhja e gjuhës informacion shtese Asnjë lidhje interneti nuk ofrohet. Ju lutemi zgjidhni gjuhën nga Vazhdo Zgjedhja e gjuhës Qasja juaj në ekip (pa pagesë): Kjo mund të ndryshohet. tuaj amtare, mund të ndihmoni që së antiX nuk është përkthyer ende në gjuhën tuaj? shpejti të përjetoni antiX në gjuhën tuaj. të shihni këtë informacion. Për ta bërë këtë, klikoni në butonin përkatës. Jeni shumë të mirëpritur të kontribuoni në përkthimet. menyja në të cilën dëshironi 