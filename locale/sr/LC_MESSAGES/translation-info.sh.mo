��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  i   �     Q  J   b     �     �  p   �     J	     ^	  6   {	  1   �	  "   �	  >   
  �   F
  [   �
     =  %   R     x  %   �  4   �  *   �          %  4   =  &   r  A   �  E   �  K   !  0   m  _   �  T   �  ,   S               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:35+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Алтернативно, можете погледати неке локалне информације. Употреба Да ли наилазите на непреведена подручја? Доступни језици: Прекид Кликните на "Настави" након што успоставите интернетску везу. Закључити  Грешка на мрежи Покажите локалне информације Да ли видите само енглески? Не приказуј поново Ако разумете мало енглеског поред Лакше је него што мислите,
а можете допринети колико год желите.
Само се региструјте. Помозите волонтерском преводилачком тиму АнтиКс, Само уради. Језичке информације избор језика Додатне Информације Интернет веза није доступна. Молимо изаберите језик Настави Избор језика Ваш приступ тиму (бесплатно): То се може променити. свог матерњег језика, можете помоћи антиКс још није преведен на ваш језик? да ускоро доживите антиКс на свом језику. да видите ове информације. Да бисте то урадили, кликните на одговарајуће дугме. Врло сте добродошли да допринесете преводима. из менија на коме желите 