��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �  *   v  �   �  5   9	     o	  �   �	     K
  +   k
  >   �
  s   �
  A   J  �   �  R    �   i  2        Q     n  %   �  3   �  H   �     1  F   P  W   �  3   �  �   #  �   �  �   u  M   �  �   J  �   �  I   Y               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:44+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 மாற்றாக, நீங்கள் சில உள்ளூர் தகவல்களைப் பார்க்கலாம். பயன்படுத்தவும் நீங்கள் மொழிபெயர்க்கப்படாத பகுதிகளை சந்திக்கிறீர்களா? கிடைக்கும் மொழிகள்: நிறுத்து நீங்கள் இணைய இணைப்பை நிறுவிய பிறகு "தொடரவும்" என்பதைக் கிளிக் செய்யவும். முடிவுக்கு  நெட்வொர்க் பிழை உள்ளூர் தகவலைக் காட்டு நீங்கள் ஆங்கிலம் மட்டுமே பார்க்கிறீர்களா? மீண்டும் காட்ட வேண்டாம் உங்கள் தாய் மொழியுடன் கூடுதலாக கொஞ்சம் ஆங்கிலத்தை நீங்கள் நினைப்பதை விட இது எளிதானது,
மேலும் நீங்கள் விரும்பும் அளவுக்கு அல்லது
குறைவாக பங்களிக்க முடியும். பதிவு செய்யுங்கள். தன்னார்வ எதிர்ப்பு எதிர்ப்பு மொழிபெயர்ப்பு குழுவுக்கு உதவுங்கள், அதைச் செய்யுங்கள். மொழி தகவல் மொழி தேர்வு கூடுதல் தகவல் இணைய இணைப்பு இல்லை. இந்த தகவலை நீங்கள் பார்க்க தொடருங்கள் மொழியைத் தேர்ந்தெடுப்பது அணிக்கான உங்கள் அணுகல் (இலவசமாக): அதை மாற்ற முடியும். நீங்கள் புரிந்து கொண்டால், உங்கள் மொழியில் ஆன்டிஎக்ஸை விரைவில் ஆன்டிஎக்ஸ் இன்னும் உங்கள் மொழியில் மொழிபெயர்க்கப்படவில்லை? அனுபவிக்க முடியும் என்பதை உறுதிப்படுத்த உதவலாம். மொழியைத் தேர்ந்தெடுக்கவும். இதைச் செய்ய, தொடர்புடைய பொத்தானைக் கிளிக் செய்க. மொழிபெயர்ப்புகளில் பங்களிக்க உங்களை வரவேற்கிறோம். விரும்பும் மெனுவிலிருந்து 