��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  �   �     �  |   �  <   	     J	  �   i	     <
  (   S
  M   |
  a   �
  1   ,  t   ^  J  �  �        �     �     �  %     a   '  E   �  $   �  +   �  T      )   u  �   �  [   "  �   ~  ,     �   =  w   �  J   8               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:44+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: te
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 ప్రత్యామ్నాయంగా, మీరు కొంత స్థానిక సమాచారాన్ని చూడవచ్చు. వా డు మీరు అనువదించని ప్రాంతాలను ఎదుర్కొంటున్నారా? అందుబాటులో ఉన్న భాషలు: గర్భస్రావం మీరు ఇంటర్నెట్ కనెక్షన్‌ను ఏర్పాటు చేసిన తర్వాత "కొనసాగించు" పై క్లిక్ చేయండి. ముగించు  నెట్వర్క్ లోపం స్థానిక సమాచారాన్ని చూపించు మీకు ఇంగ్లీష్ మాత్రమే కనిపిస్తుందా? మళ్లీ చూపించవద్దు మీరు మీ మాతృభాషతో పాటు కొద్దిగా ఇంగ్లీషును మీరు అనుకున్నదానికంటే ఇది చాలా సులభం,
మరియు మీరు కోరుకున్నంత ఎక్కువ లేదా తక్కువ
సహకారం అందించవచ్చు. కేవలం నమోదు చేసుకోండి. స్వచ్ఛంద యాంటీఎక్స్ అనువాద బృందానికి సహాయం చేయండి, అది చేయండి. భాష సమాచారం భాష ఎంపిక అదనపు సమాచారం ఇంటర్నెట్ కనెక్షన్ అందుబాటులో లేదు. దయచేసి మీరు ఈ సమాచారాన్ని కొనసాగించండి భాషను ఎంచుకోవడం బృందానికి మీ యాక్సెస్ (ఉచితంగా): అది మార్చవచ్చు. అర్థం చేసుకుంటే, మీరు త్వరలో మీ భాషలో యాంటిఎక్స్ antiX ఇంకా మీ భాషలోకి అనువదించబడలేదా? అనుభవిస్తారని నిర్ధారించుకోవడానికి మీరు సహాయపడగలరు. భాషను ఎంచుకోండి. దీన్ని చేయడానికి, సంబంధిత బటన్‌పై క్లిక్ చేయండి. అనువాదాలకు సహకరించడానికి మీకు చాలా స్వాగతం. చూడాలనుకుంటున్న మెను నుండి 