��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  x   �     `  Y   q      �     �  q   	     t	     �	  ;   �	  H   �	      0
  Z   Q
  �   �
  V   �     �            !   ;  :   ]  1   �  &   �     �  ;     3   I  Z   }  N   �  Q   '  )   y  L   �  H   �  4   9               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:44+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Интихобан, шумо метавонед ба баъзе маълумоти маҳаллӣ нигоҳ кунед. Истифода Оё шумо бо минтақаҳои тарҷумашуда дучор мешавед? Забонҳои дастрас: Қатъ кардан Пас аз таъсиси пайвасти интернетӣ "Давом додан" -ро клик кунед. Хулоса кардан  Хатогии шабака Маълумоти маҳаллиро нишон диҳед Оё шумо танҳо забони англисиро мебинед? Боз нишон надиҳед Агар шумо илова ба забони модарии худ каме забони Ин аз оне ки шумо мепиндоред, осонтар аст
ва шумо метавонед ҳарчӣ бештар ё камтар ҳиссаи
худро гузоред. Танҳо сабти ном кунед. Ба гурӯҳи ихтиёрии тарҷумаи зиддиX кӯмак кунед, Бигиру бикун. Маълумоти забон интихоби забон Маълумоти Иловагӣ Пайвасти интернет дастрас нест. Лутфан аз менюе, ки мехоҳед Рафтанро давом диҳед Интихоби забон Дастрасии шумо ба даста (ройгон): Инро тағир додан мумкин аст. англисиро дарк кунед, шумо метавонед кумак кунед, antiX ҳанӯз ба забони шумо тарҷума нашудааст? ки шумо ба зудӣ бо забони худ antiX эҳсос кунед. забонро интихоб кунед. Барои ин кор, тугмаи мувофиқро клик кунед. Хуш омадед ба саҳмгузорӣ дар тарҷумаҳо. ин маълумотро дидан мехоҳед, 