��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  ?   �  	   '  2   1     d     s  <   y     �     �     �  "   �     	  -   !	  r   O	  .   �	     �	     �	     	
     
     
     ;
     P
     \
     h
     �
  5   �
  +   �
  )         *  .   B  +   q     �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:37+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Alternatif olarak, bazı yerel bilgileri kontrol edebilirsiniz. Kullanmak Çevrilmemiş alanlarla mı karşılaşıyorsunuz? Mevcut diller: iptal İnternet bağlantısı kurduktan sonra "Devam"a tıklayın. Sonuçlandırmak Ağ hatası Yerel bilgileri göster Sadece İngilizce mi görüyorsun? bir daha gösterme Ana dilinizin yanı sıra biraz da İngilizce Düşündüğünüzden daha kolay ve
istediğiniz kadar az ya da çok katkıda
bulunabilirsiniz. Sadece kayıt ol. Gönüllü antiX çeviri ekibine yardım edin, Sadece yap. Dil bilgisi Dil seçimi ek bilgi İnternet bağlantısı yok. Lütfen bu bilgileri Devam etmek Dil seçimi Ekibe erişiminiz (ücretsiz): Bu değiştirilebilir. anlıyorsanız, kısa sürede antiX'i kendi dilinizde antiX henüz sizin dilinize çevrilmedi mi? deneyimlemenize yardımcı olabilirsiniz. menüden dili seçiniz. Bunu yapmak için ilgili düğmeyi tıklayın. Çevirilere katkıda bulunmanız çok hoş. görmek istediğiniz  