��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  f   �     N  O   _  5   �  !   �  k   	     s	     �	  .   �	  7   �	      
  ;   
  �   Z
  b   �
     O     W     k          �     �     �     �  5   	  -   ?  <   m  I   �  m   �  +   b  C   �  W   �     *               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 18:46+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 ئۇنىڭدىن باشقا ، سىز بەزى يەرلىك ئۇچۇرلارنى كۆرەلەيسىز. ئىشلىتىش تەرجىمە قىلىنمىغان رايونلارغا يولۇقامسىز؟ ئىشلەتكىلى بولىدىغان تىللار: ئەمەلدىن قالدۇرۇش تور ئۇلىنىشى ئورناتقاندىن كېيىن «داۋاملاشتۇرۇش» نى چېكىڭ. خۇلاسە  تور خاتالىقى يەرلىك ئۇچۇرلارنى كۆرسەت سىز پەقەت ئىنگلىزچە كۆرەمسىز؟ قايتا كۆرسەتمەڭ ئەگەر سىز ئانا تىلىڭىزدىن باشقا  بۇ سىز ئويلىغاندىن ئاسان
، سىز خالىغانچە ياكى ئاز
تۆھپە قوشالايسىز. تىزىملىتىڭ. پىدائىيلارغا قارشى X تەرجىمە ئەترىتىگە ياردەم قىلىڭ ، قىل. تىل ئۇچۇرى تىل تاللاش قوشۇمچە ئۇچۇرلار تور ئۇلىنىشى يوق. بۇ ئۇچۇرلارنى  داۋاملاشتۇرۇڭ تىل تاللاش كوماندىغا كىرىشىڭىز (ھەقسىز): بۇنى ئۆزگەرتىشكە بولىدۇ. ئازراق ئىنگلىزچىنى چۈشەنسىڭىز ،  antiX تېخى تىلىڭىزغا تەرجىمە قىلىنمىدىمۇ؟ تىلىڭىزدا antiX نى تېزلا ھېس قىلالىشىڭىزغا ياردەم بېرەلەيسىز. تىزىملىكتىن تىل تاللاڭ. بۇنىڭ ئۈچۈن مۇناسىپ كۇنۇپكىنى بېسىڭ. تەرجىمىگە تۆھپە قوشقانلىقىڭىزنى قارشى ئالىمىز. كۆرمەكچى بولغان  