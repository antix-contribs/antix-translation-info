��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  i   �     Q  T   n     �     �  }   �      o	     �	  8   �	  =   �	  &   #
  Y   J
  �   �
  ^   r     �  $   �       '   (  :   P  .   �     �     �  A   �     #  Z   C  >   �  ]   �  '   ;  G   c  D   �  .   �               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:38+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 Крім того, ви можете перевірити деяку місцеву інформацію. Використовуйте Чи стикаєтесь ви з неперекладеними областями? Доступні мови: Скасувати Натисніть "Продовжити" після встановлення підключення до Інтернету. Зробіть висновок  Помилка мережі Показувати місцеву інформацію Ви бачите тільки англійську мову? Не показувати більше Якщо ви трохи розумієте англійську на додаток до Це простіше, ніж ви думаєте, і ви
можете внести свій внесок скільки завгодно
або так мало. Просто зареєструйтесь. Допоможіть волонтерській команді перекладачів antiX, Просто зроби це. Інформація про мову вибір мови Додаткова інформація Немає підключення до Інтернету. Будь ласка, виберіть мову Продовжуй Вибір мови Ваш доступ до команди (безкоштовно): Це можна змінити. рідної мови, ви можете допомогти переконатися, що antiX ще не перекладено на вашу мову? незабаром зможете випробувати антиХ на вашій мові. бачити цю інформацію. Для цього натисніть відповідну кнопку. Запрошуємо долучитися до перекладів. з меню, на якому ви хочете 