��             +         �  1   �     �  "        $     9  :   @     {     �     �     �     �  4   �  B     *   N     y     �     �     �  !   �  #   �                    .  ;   B  7   ~  )   �     �  *   �  8   #  #   \  f  �  3   �       '   "     J     ]  -   d     �     �     �     �     �  $   �  �   	  ,   �	     �	     �	     �	     �	     �	     
     
     %
  $   2
     W
  *   v
  )   �
  &   �
     �
  0        9     R               
                                                                       	                                                          Alternatively you may view some local information Apply Are you facing untranslated parts? Available languages: Cancel Click »Procede« after having established the connection. Close Connection error Display local Information Do you see English only? Don't show again In case you understand apart from your native tongue It is not difficult, and you can do as much or little as you like. Join antiX translation team of volunteers, Just do it. Language information Language selection More Information No internet connection available. Please select the language in which Procede Select Language Sign up for free: This can be helped. a bit of English also, you can contribute a bit, so you can antiX is not translated to your favourite language yet? experience antiX in your own tongue soon. from the pulldown menu. instead by pressing the respective button. you are always welcome to take part in the translations. you d like to read this information Project-Id-Version: antiX translation-info ver. 0.7
Report-Msgid-Bugs-To: forum.antixlinux.com
PO-Revision-Date: 2021-09-24 00:39+0200
Language-Team: https://www.transifex.com/antix-linux-community-contributions/
Language: zh
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
X-Generator: Poedit 2.3
 另外，你也可以查看一些当地的信息。 申请 你是否遇到过未翻译的部分？ 可用的语言。 取消 建立互联网连接后，点击 "继续"。 关闭 网络错误 查看当地信息 你只看到英文吗？ 不要再显示 如果你除了母语之外还懂一 这比你想象的要容易，
而且你可以根据自己的意愿贡献出更多或更少的力量。
只要注册就可以了。 在志愿者antiX翻译组中提供帮助。 就这样做吧。 语言信息 语言选择 更多信息 没有网络连接可用。 请从菜单中 继续 选择语言 你对团队的访问（免费）。 这一点是可以改变的。 点英语，你可以帮助确保你很快 antiX还没有被翻译成你的语言？ 就能用你的语言体验到antiX。 的信息的语言。 要做到这一点，请点击相应的按钮。 欢迎你参与翻译。 选择你想看到 